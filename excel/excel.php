<?php

require("database/db.php");

function getExcelData($file_tmp, $filename) {
   	move_uploaded_file($file_tmp, $filename);

	$inputFileName  =   $filename;
	$inputFileType = 'Xlsx';
	$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
	$spreadsheet = $reader->load($inputFileName);
	$worksheet = $spreadsheet->getActiveSheet();
	$rows = $worksheet->toArray();

	$i = 0;
	$c = 0;
	$j = 0;

		
	$servername = "localhost";      
	$username = "root";
	$password = "";
	$db = "pare";

	$conn = mysqli_connect($servername, $username, $password,$db);

	foreach($rows as $key => $value) {

		$i++;
		$j++;

		if($i==1)
			continue;
		$name = str_replace("'",'"', $value[0]);
		$surname = str_replace("'",'"', $value[1]);
		$email = $value[2];
		$phone = $value[3];
		$category = $value[4];
		$last_visit = $value[5];
		//questa parte ragiona impostando la data come da listato in basso
		$last_visit = getFormatDate($last_visit, $i); 
		$note = '';
		if(!empty($value[10]))
		$note = str_replace("'",'"',$value[10]);
		$next = getFormatDate($value[11], $i);
	
        $check = "SELECT * FROM `scadenzario` WHERE email LIKE '$email'";
		$isPresent = select($check, $conn);
		if(count($isPresent) == 0)  {
			$sql = "INSERT INTO `scadenzario` (`ID`, `name`, `surname`, `category`, `last_visit`, `note`, `email`, `phone`, `next`) VALUES (NULL, '$name', '$surname', '$category', '$last_visit', '$note', '$email', '$phone', '$next');";
			query($sql, $conn);
			$c++;
		}
		
		else
		{$aggiornamento = "UPDATE scadenzario set name = '$name' WHERE 'email' LIKE '$email'";
	    query($aggiornamento, $conn);
        $c++;
		}

		//questa parte è provvisoria, serve per fermare il ciclo a 3 nomi inseriti e poi fermarsi
		if($j == 4)               
			return $c;
 
	};
	return $c;
}

function getFormatDate($str, $i) {
	if(empty($str)) {
		die("Errore di parsing per la data di ultima visita: $str, riga $i");
	}
	$parts = explode("/", $str);
	if(count($parts) == 0) {
		die("Errore di parsing per la data di ultima visita: $str, riga $i");
	}
	$day = $parts[1];
	$month = $parts[0];             
	$year = $parts[2];
	$formatted = $year . "-" . $month . "-" . $day;     //modificato da / perché MySQL riceve solo il formato YYYY-MM-DD
	return $formatted;
}

