<style>
  .message {
    color: white;
  }
</style>
<?php

require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Twilio\Rest\Client;

require("excel/excel.php");
require("notification/notification.php");

$data = [];
$message = "";

if(isset($_FILES['input'])) {
   $errors= array();
   $file_name = $_FILES['input']['name'];
   $file_tmp =$_FILES['input']['tmp_name'];
   
   if(empty($errors)==true) {
   	$data = getExcelData($file_tmp,"input/".$file_name);
      $message = "Numero righe processate = $data";

   }
}

?>
 <link rel="stylesheet" media="screen" href="style.css">
<div class='box'>
  <div class='box-form'>
    <div class='box-login-tab'></div>
    <div class='box-login-title'>
      <div class='i i-login'></div>
      <h2>IMPORTAZIONE</h2>
    </div>
    <form action="" method="POST" enctype="multipart/form-data">
    <div class='box-login'>
      <div class='fieldset-body' id='login_form'>
        <p class='field'>
          <label style="text-align: center;" for='user'>Excel</label>
          <input type="file" name="input" accept=".xml, .xlsx" />
          <span id='valida' class='i i-warning'></span>
        </p>

        <input type='submit' id='do_login' value='Upload' title='Upload' />
      </div>
    </div>
   </div>
    </form>
  <div class="message"><?php echo  $message;?></div>
  </div>
</div>

<?php
