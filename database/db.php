<?php
	//questi sono i dati richiesti per l'accesso al database, al momento lavoriamo in locale con PhpMyAdmin

function select($sql, $conn) {
	$result = $conn->query($sql);
	$rows = [];
	if ($result->num_rows > 0) {
	  while($row = $result->fetch_assoc()) {
	    $rows []= $row;
	  }
	}
	return $rows;
}

function query($sql, $conn) {
	if ($conn->query($sql) != TRUE) {
	  echo "Error: " . $sql . "<br>" . $conn->error;
	}
}